export class FizzBuzz {

    esDivisible (dividendo, divisor){
        return dividendo % divisor == 0;
    }

    quePalabraPoner(numero){
        var esDivisiblePor3 = this.esDivisible(numero,3);
        var esDivisiblePor5 = this.esDivisible(numero,5);

        if (esDivisiblePor3 && esDivisiblePor5){
            return 'FizzBuzz'
        }
        else if(!esDivisiblePor3 && !esDivisiblePor5){
            return numero.toString();
        }
        else if (esDivisiblePor3){
            return 'Fizz'
        }
        else if (esDivisiblePor5){
            return 'Buzz'
        }
    }
}