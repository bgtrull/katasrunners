import { MarsRover } from '../src/marsRover.js'

const N = 0
const E = 1
const S = 2
const W = 3
const DIMENSION = 5

describe('MarsRover', () => {

    describe('Yellow belt', () => {

        it('Mars Rover returns its position', () => {
            // Arrange
            const marsRover = new MarsRover(5, 5, N)
        
            let expectedPosition = [5, 5, N]

            // Act
            let getPosition = marsRover.getPosition()     

            // Assert
            expect(getPosition).toEqual(expectedPosition)
        })

        it('Mars Rover receives commands', () => {
            // Arrange
            const marsRover = new MarsRover(1, 2, N, 'MR')
            let expectedCommand = 'MR'

            // Act
            let commands = marsRover.getCommands()

            // Assert
            expect(commands).toEqual(expectedCommand)
        })

        it('Mars Rover receives string commands', () => {
            // Arrange
            const marsRover = new MarsRover(1, 2, N, 'M') 
            let expectedCommand = 'M'

            // Act
            let commands = marsRover.getCommands()
                    
            // Assert
            expect(commands).toEqual(expectedCommand)
            expect(typeof commands).toEqual('string')
        })
    })

    describe('Green belt', () => {

        it('Mars Rover receives M command and moves forward', () => {
            // Arrange
            const marsRover = new MarsRover(1, 2, N, 'MMM') 
            let expectedPosition = [1, 5, N]

            // Act
            let position = marsRover.getPosition()
                    
            // Assert
            expect(position).toEqual(expectedPosition)
        })

        it('When the rover recives the command "R" it turns right', () => {
            // Arrange
            const marsRover = new MarsRover(1, 2, N, "R") 
            let expectedPosition = [1, 2, E]

            // Act
            let position = marsRover.getPosition()
                    
            // Assert
            expect(position).toEqual(expectedPosition)
        })

        it('When the rover recives the command "L" it turns left', () => {
            // Arrange
            const marsRover = new MarsRover(1, 2, N, "L") 
            let expectedPosition = [1, 2, W]

            // Act
            let position = marsRover.getPosition()
                    
            // Assert
            expect(position).toEqual(expectedPosition)
        })

        it('When the rover recives the multiple commands it does what it has to do', () => {
            // Arrange
            const marsRover = new MarsRover(0, 0, N, "LMMRMML") 
            let expectedPosition = [-2, 2, W]

            // Act
            let position = marsRover.getPosition()
                    
            // Assert
            expect(position).toEqual(expectedPosition)
        })
    })

    describe('Red belt', () => {

        xit('Rover knows world dimensions', () => {
            // Arrange
            const marsRover = new MarsRover(0, 5, N, "MLM") 
            let expectedPosition = [5, 0, W]

            // Act
            let position = marsRover.getPosition()
                    
            // Assert
            expect(position).toEqual(expectedPosition)
        })
    })
})

