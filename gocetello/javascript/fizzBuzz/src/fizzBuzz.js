export function fizzBuzz(range = 100) {
    const list = []
    for (let num = 1; num <= range; num++) {

        const fizzBuzzNumber = new myNumber(num)
        list.push(fizzBuzzNumber.getValue())
    }
    return list;
}

class myNumber {
    constructor(number){
        this.number = number
    }
    
    getValue(){
        const rules = [
            [3, 'Fizz'],
            [5, 'Buzz'],
        ]
        let value = ''

        rules.forEach(rule => {
            const num = rule[0]
            const word = rule[1]
            const verified = this.divisibleBy(num) || this.contains(num)
            value +=  this.getWord(verified, word)
        });
        
        return {
            true: this.number.toString(),
            false: value
        }[this.isEmpty(value)]
    }

    isEmpty(value){
        return value == ''

    }

    getWord(veredict, word){
        return {
            true: word,
            false: ''
        }[veredict]
    }

    divisibleBy(divisor){
        return this.number % divisor == 0 
    }

    contains(num){
        const splitNumber = this.number.toString().split('')
        return splitNumber.includes(num.toString())
    }
}



