export class Tennis {
    constructor() {
        this.score_player1 = {
            sets: 'Love',
            games: 'Love',
            points: 'Love'
        }

        this.score_player2 = {
            sets: 'Love',
            games: 'Love',
            points: 'Love'
        }

    }

    getScores() {
        return [this.score_player1, this.score_player2]
    }
}