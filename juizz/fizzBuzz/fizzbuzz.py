def fizzbuzz(size_list=100):
    return [FizzBuzz(x).generate_value() for x in range(1,size_list + 1)]

class FizzBuzzList(list):
    def __init__(self, size_list=100):
        self.size_list = size_list
        self.get_list()

    def get_list(self):
        for x in range(1,self.size_list + 1):
            self.append(FizzBuzz(x).generate_value())

class FizzBuzz:
    def __init__(self, x):
        self.mynumber = MyNumber(x)
        self.mystring = MyString(x)

    def generate_value(self):
        my_dict = {
            self.substitute_by_fizz(): self.mystring.get_fizz(),
            self.substitute_by_buzz(): self.mystring.get_buzz(),
            self.substitute_by_fizzbuzz(): self.mystring.get_fizzbuzz(),
        }
        return my_dict.get(True, self.mystring)
    
    def substitute_by_fizzbuzz(self):
        return self.mynumber.is_divisible_by_five() and self.mynumber.is_divisible_by_three()
        
    def substitute_by_fizz(self):
        return self.mynumber.is_divisible_by_three() or self.mynumber.contains_three()
    
    def substitute_by_buzz(self):
        return self.mynumber.is_divisible_by_five() or self.mynumber.contains_five()
        

class MyString(str):
    def get_fizz(self):
        return 'Fizz'
    def get_buzz(self):
        return 'Buzz'
    def get_fizzbuzz(self):
        return 'FizzBuzz'
    

class MyNumber(int):
    def contains_five(self):
        return '5' in MyString(self)

    def contains_three(self):
        return '3' in MyString(self)

    def is_divisible_by_three(self):
        return not self%3

    def is_divisible_by_five(self):
        return not self%5