from functools import *

class StringNumbers:
    def __init__(self, string):
        if '-' in string:
            raise TypeError('negatives are not allowed')

        self.string = string
        self.delimiters = ['\n', ',']
        self.update_delimiters()
        self.string_numbers = self.get_string_numbers()

    def get_string_numbers(self):
        string_numbers = self.string
        if self.string_with_delimiters():
            string_numbers = self.string.split('\n')[1]
        return string_numbers

    def get_numbers(self):
        formatted_string = self.standarize_delimiter(self.string_numbers)
        array = formatted_string.split(' ')
        return list(map(lambda x: int(x), array))

    def update_delimiters(self):
        if self.string_with_delimiters():
            self.delimiters.append(self.string[2])

    def string_with_delimiters(self):
        return self.string and self.string[0] == '/'

    def standarize_delimiter(self, string):
        for delimiter in self.delimiters:
            string = string.replace(delimiter, ' ')
        return string

    def calculate_total(self):
        if not self.string:
            return 0

        numbers = self.get_numbers()
        return reduce(lambda a,b: a + b, numbers)

def string_calculator(string):
    parser = StringNumbers(string)
    return parser.calculate_total()
