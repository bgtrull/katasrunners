export function convertToRoman(num) {

    const ROMAN_NUMBERS = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]
    const DECIMAL_NUMBERS = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]

    let romanized = ""

    for (let idx = 0; idx < DECIMAL_NUMBERS.length; idx++) {
        while (num >= DECIMAL_NUMBERS[idx]){
            romanized += ROMAN_NUMBERS[idx]
            num -= DECIMAL_NUMBERS[idx]
        }
    }

    return romanized;
}