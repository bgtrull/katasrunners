
export class DoomSimulator {
    constructor () {
        this.player = new Player()
        this.level_demons = []
        this.player_weapons =  [{name:"Fists", ammo:0, dmg:30}]
    }

    game(){
        this.getLevel()
        this.startAttack()
    }

    getLevel() {
        this.player = new Player()
        this.obtainDemons()
        this.obtainWeapons()
    }

    startAttack() {
        let random_demon = Math.floor(Math.random()*this.level_demons.length)

        if(this.level_demons[random_demon] == undefined) {
                console.log("YOU WON! NEXT LVL")
                this.game() 
        }

        const DEMON_TOTAL_HP = this.level_demons[random_demon]
        console.log("DEMON TOTAL HP:", DEMON_TOTAL_HP)

        console.log("DEMONS:", this.level_demons)
        console.log("DEMON:", this.level_demons[random_demon])

        while (this.level_demons[random_demon].hp > 0 && this.player.isAlive()) {
            let random_weapon = Math.floor(Math.random()*this.player_weapons.length)
           
            console.log("WEAPON:", this.player_weapons[random_weapon].name)
            console.log("HP DEMON:", this.level_demons[random_demon].hp)
            console.log("PLAYER HP:", this.player.health, "ARMOR:",this.player.armor)

            if (this.player_weapons.length == 1 || (this.player_weapons.length == 2 && this.player_weapons.includes("Chainsaw"))) {
                this.level_demons[random_demon].hp -= this.player_weapons[random_weapon].dmg
                this.takeDamage(this.level_demons[random_demon].dmg)

            } else if (this.player_weapons[random_weapon].name == "Fists" || this.player_weapons[random_weapon].name == "Chainsaw" ) {
                random_weapon++

            } else if (this.player_weapons[random_weapon].ammo == 0) {
                this.player_weapons.splice(random_weapon, 1)

            } else {
                this.level_demons[random_demon].hp -= this.player_weapons[random_weapon].dmg
                this.player_weapons[random_weapon].ammo -= this.player_weapons[random_weapon].shot
                this.takeDamage(this.level_demons[random_demon].dmg)
            }
        }

        console.log("DEMON TOTAL HP:", DEMON_TOTAL_HP)
     
        console.log("WEAPON STATS:", this.player_weapons)
       
        if (!this.player.isAlive()) {
            console.log("GAME OVER")
            return "GAME OVER"
        } else {
            console.log("NEXT ROUND")
            this.player.increaseArmor(Math.round(DEMON_TOTAL_HP * 0.01))
            this.player.increaseHealth(Math.round(DEMON_TOTAL_HP * 0.02))
            this.level_demons.splice(random_demon, 1)
            this.obtainDrop()
            this.startAttack()
        }

    }
       
    obtainWeapons() {
        const WEAPONS = [
            {name:"Chainsaw", ammo:0, dmg:30},
            {name:"Pistol", ammo:200, shot:1, dmg:15},
            {name:"Shotgun", ammo:30, shot:1, dmg:50},
            {name:"Chaingun", ammo:200, shot:10, dmg:150},
            {name:"Rocket launcher", ammo:20, shot:1, dmg:200},
            {name:"Plasma gun", ammo:200, shot:10, dmg:200},
            {name:"BFG 9000", ammo:200, shot:50, dmg:1000}
        ]
       
        for (let idx = 0; idx < 3; idx++) {
            if (this.player_weapons.length == 5){
                return
            }

            let random_weapon = WEAPONS[Math.floor(Math.random()*7)]

            if (this.player_weapons.includes(random_weapon)) {
                idx--
            } else {
                this.player_weapons.push(random_weapon)
            }
        }
    }

    obtainDemons() {
        const DEMONS = [
            {name:"Zombie", hp:50, dmg:4},
            {name:"Shotgun_guy", hp:60, dmg:6},
            {name:"Imp", hp:70, dmg:8},
            {name:"Demon", hp:80, dmg:10},
            {name:"Spectre", hp:80, dmg:10},
            {name:"Lost_Soul", hp:45, dmg:6},
            {name:"Cacodemon", hp:215, dmg:10},
            {name:"Baron_of_Hell", hp:230, dmg:12},
            {name:"Cyberdemon", hp:500, dmg:16},
            {name:"Spider_Mastermind", hp:600, dmg:18}
        ]
        for (let idx = 0; idx < 6; idx++) {
            this.level_demons.push(DEMONS[Math.floor(Math.random()*10)])
        }
    }
    
    takeDamage(damage) {
        this.player.takeDamage(damage)
    }

    obtainDrop() {
        const DEMONS_DROP = ["Nothing", "Armor", "Health", "Bullets", "Shells", "Rocket", "Plasma"]
        const RANDOM_DROP = DEMONS_DROP[Math.floor(Math.random()*7)]
        if (RANDOM_DROP == "Armor") {
            this.player.increaseArmor(10)
        }
        if (RANDOM_DROP == "Health") {
            this.player.increaseHealth(20)
        }
    }
}
/*Nada
- 10 Armadura
- 20 de vida
- 20 balas
- 5 cartuchos
- 1 Misil
- 20 de plasma 
*/
class Player {
    constructor() {
        this.health = 100
        this.armor = 50
    }

    isAlive() {
        return this.health > 0
    }

    increaseArmor(armor) {
        this.armor += armor

        if (this.armor > 50) {
            this.armor = 50
        }
    }

    increaseHealth(health) {
        this.health += health

        if (this.health > 100) {
            this.health = 100
        }
    }

    takeDamage(damage) { 
        this.armor -= damage
        if (this.armor >= 0) {
            return
        }
        const remaining_damage = this.armor * -1
        this.armor = 0
        this.health -= remaining_damage
    }
}