class Rule {
    constructor(number){
        this.number = number

    }
}

class IsFizz extends Rule {
    applies() {
        return this.number % 3 == 0
    }

    output() {
        return "Fizz"
    }
}

class IsBuzz extends Rule {
    applies() {
        return this.number % 5 == 0
    }

    output() {
        return "Buzz"
    }
}

class IsFizzBuzz extends Rule {
    applies() {
        return this.number % 5 == 0 && this.number % 3 == 0
    }

    output() {
        return "FizzBuzz"
    }
}

class IsNoneOfTheAbove extends Rule {
    applies() {
        return true
    }

    output() {
        return this.number.toString()
    }
}

export function fizzbuzz(number) {
    const rules = [
        new IsFizzBuzz(number),
        new IsFizz(number),
        new IsBuzz(number),
        new IsNoneOfTheAbove(number),
        
    ]

    for (let rule of rules) {
        if (rule.applies()) {
            return rule.output()
        }
    }

}

/*
export function fizzbuzz(variable) {

    const FIZZ = "Fizz"
    const BUZZ = "Buzz"

    if (isFizz(variable) && isBuzz(variable)){
        return FIZZ+BUZZ;
    } else if (isFizz(variable)){
        return FIZZ;
    } else if (isBuzz(variable)){
        return BUZZ;
    } else {
        return variable.toString();
    }
}

function isFizz(variable){
    return variable % 3 == 0 
}    
function isBuzz(variable){
    return variable % 5 == 0 
}
*/