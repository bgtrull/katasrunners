require_relative 'caesar_cypher'
require 'test/unit'

class TestCaesarCypher < Test::Unit::TestCase
  def test_a_replaces_single_letter
    raw_text = 'A' 
    expected_result = 'N'

    actual_result = rot13(raw_text)

    assert_equal(expected_result, actual_result)
  end

  def test_b_starts_from_the_beginning_if_the_letter_is_at_the_end_of_the_alphabet
    raw_text = 'R' 
    expected_result = 'E'

    actual_result = rot13(raw_text)

    assert_equal(expected_result, actual_result)
  end

  def test_c_replaces_a_single_word_with_the_cyphered_one
    raw_text = 'FREE' 
    expected_result = 'SERR'

    actual_result = rot13(raw_text)

    assert_equal(expected_result, actual_result)
  end

  def test_d_ignores_spaces_when_cyphering_a_sentence
    raw_text = 'FREE PIZZA' 
    expected_result = 'SERR CVMMN'

    actual_result = rot13(raw_text)

    assert_equal(expected_result, actual_result)
  end

  def test_e_ignores_non_alphanumeric_characters
    raw_text = 'FREE PIZZA?!' 
    expected_result = 'SERR CVMMN?!'

    actual_result = rot13(raw_text)

    assert_equal(expected_result, actual_result)
  end
end
