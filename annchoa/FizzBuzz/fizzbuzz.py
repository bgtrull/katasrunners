class FizzBuzz():
    def __init__(self, rules):
        self.rules = rules

    def fizz_buzz(self, number):
        for rule in self.rules:
            rule.set_number(number)
            if rule.complies():
                return rule.replacement()

class Rule():
    def set_number(self, number):
        self.number = number

    def complies(self):
        return False

    def replacement(self):
        return ''

class FizzRule(Rule):
    def complies(self):
        return self.number % 3 == 0 or '3' in str(self.number)

    def replacement(self):
        return 'Fizz'

class BuzzRule(Rule):
    def complies(self):
        return self.number % 5 == 0 or '5' in str(self.number)

    def replacement(self):
        return 'Buzz'

class FizzBuzzRule(Rule):
    def set_number(self, number):
        self.fizz = FizzRule()
        self.fizz.set_number(number)
        
        self.buzz = BuzzRule()
        self.buzz.set_number(number)

    def complies(self):
        return self.fizz.complies() and self.buzz.complies()

    def replacement(self):
        return self.fizz.replacement() + self.buzz.replacement()

class EverythingElseStringRule(Rule):
    def complies(self):
        return True

    def replacement(self):
        return str(self.number)
