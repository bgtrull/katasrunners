export function palindrome(aString) {

    let unify_string = aString.replace(/[\W_]/g, "").toLowerCase()
    return reverseString(unify_string) == unify_string
  }

  function reverseString(unify_string) {
    let reversed = ''
    for (let index = unify_string.length-1; index >= 0; index--){
      reversed += unify_string[index]
    }
    return reversed
  }
