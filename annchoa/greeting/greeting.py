class Greet():
    hello = "Hello, "
    dot = "."

    def sentence(self, list_name):
        if self.emptyList(list_name):
            return  "Hello, my friend."

        names = []
        for name in list_name:
            multiple_names = name.split(', ')
            names.extend(multiple_names)

        upperName = list(filter(lambda name: name.upper() in name, names))
        upperOutput = self.upperGreet(upperName)

        capitalizeName = list(filter(lambda name: name.capitalize() in name, names))
        capitalizedOutput = self.normalGreet(capitalizeName)

        output = ""

        if capitalizedOutput != "":
            output += capitalizedOutput

        if upperOutput != "":
            if output != "":
                output += " AND "
            output += upperOutput

        return output


    def emptyList(self, list_name):
        return list_name == []

    def joinNames(self, names):
        return ", ".join(names)

    def upperGreet(self, names):
        return self.constructGreetSentence(names, self.hello.upper(), " AND ", "!")

    def normalGreet(self, names):
        return self.constructGreetSentence(names, self.hello, " and ", self.dot)

    def constructGreetSentence(self, names, greeting, glue, ending):
        if self.emptyList(names):
            return ""

        last_name = names.pop()
        output = greeting
        if names != []:
            all_names = self.joinNames(names)
            output += all_names + glue

        output += last_name + ending
        return output
