import { FizzBuzz } from '../src/fizz_buzz.js'

describe('fizzBuzz', () => {

    it('should return Fizz when is multiple of 3', () => {
      // Arrange
      const fizz_buzz = new FizzBuzz
      const expected_value = "Fizz"
      // Act
      const number = fizz_buzz.get_string(9)
      // Assert
      expect(number).toBe(expected_value)
    })

    it('should return Buzz when is multiple of 5', () => {
      // Arrange
      const fizz_buzz = new FizzBuzz
      const expected_value = "Buzz"
      // Act
      const number = fizz_buzz.get_string(25)
      // Assert
      expect(number).toBe(expected_value)
    })

    it('should return FizzBuzz when is multiple of 3 and 5', () =>{
      // Arrange
      const fizz_buzz = new FizzBuzz
      const expected_value = "FizzBuzz"
      // Act
      const number = fizz_buzz.get_string(15)
      // Assert
      expect(number).toBe(expected_value)
    })

   it('should return string representation of the number when is not divisible by 3 or 5', () =>{
      const fizz_buzz = new FizzBuzz
      const expected_value = "7"
      const number = fizz_buzz.get_string(7)
      expect(number).toBe(expected_value)
    })

    it('should return Fizz because has a 3 on it')
    const fizz_buzz = new FizzBuzz
    const expected_value = "Fizz"
    const number = fizz_buzz.get_string(43)
})
